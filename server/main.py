#! /bin/python
# -*- coding:utf-8 -*-
#
# Copyright (C) 2019 shyshell author (Sangsoic)
# 
# This program is free software: you can redistribute it and/or modify it under the terms of the 
# GNU General Public License as published by the Free Software Foundation, either version 3 of the 
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with this program.
# If not, see <https://www.gnu.org/licenses/>.

try:
	from server import Server
	import sys;
except Exception as ModuleError:
	print("[ERROR] : [MAIN] : [MSG:failed to import module] : [CAUSE:%s]" % (ModuleError));
	exit(1);

def main(argv=sys.argv):
	"""
	Central calling point of all instructions and the only routine called inglobal scale
	Args:
	  argv: calling script processus given arguments (Default value = sys.argv)

	Returns:

	"""
	srv = Server();
	srv.cli_run();

if __name__ == "__main__":
	try:
		main();
	except KeyboardInterrupt:
		exit(0);
