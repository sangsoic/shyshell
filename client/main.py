#! /bin/python3
# -*- coding:utf-8 -*-
#
# Copyright (C) 2019 shyshell author (Sangsoic)
# 
# This program is free software: you can redistribute it and/or modify it under the terms of the 
# GNU General Public License as published by the Free Software Foundation, either version 3 of the 
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with this program.
# If not, see <https://www.gnu.org/licenses/>.

import sys;
from client import Client


def main(argv=sys.argv):

	servers_ip_address = ["127.0.0.1"]; # The client will sequentially try to connect to each server ip stored in this list
	servers_ports = [8765]; # Each port will be bind to each ip address of the same list index.  




	client = Client(servers_ip_address, servers_ports);
	client.run();


if __name__ == "__main__":
	main();
